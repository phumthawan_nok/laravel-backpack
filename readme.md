# Backpack\Demo

Laravel BackPack's demo, which includes all Backpack packages.



## Install

1) Start Applications
```bash
docker-compose up -d
docker exec -u root -it backpack-demo chown -R www-data:www-data /var/www/html
```

2) Run composer install
```bash
docker exec -it backpack-demo composer install
```
or
```bash
docker exec -it backpack-demo composer update
```

3) Generate key
```bash
docker exec -it backpack-demo php artisan key:generate
```

4) migrate database
docker exec -it backpack-demo php artisan migrate
docker exec -it backpack-demo php artisan db:seed --class="Backpack\Settings\database\seeds\SettingsTableSeeder"
docker exec -it backpack-demo php artisan db:seed


## Usage 
Demo app http://localhost:8888/admin
phpmyadmin http://localhost:8408
```bash
Username : admin@example.com
Password : admin
```


